import { DtoAppCompany } from './dto-app-company';

export class DtoAppUser {
       userPk: number;
    username: string;
    firstname: string;
    lastname: string;
    emailAddress: string;
    cellNumber: string;
    company: DtoAppCompany;
    preferredBusinessEntityId: number;
    preferredLanguage: string;
    selected_company: number;
    isInternal: boolean;
    isCRFApprover: boolean;
    isCRFRequestor: boolean;
    disclaimerDate: Date;
    hasAgi: boolean;
}
