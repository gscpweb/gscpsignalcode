import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import {Subject} from 'rxjs/Subject';
import { GlobalService } from '../../shared/services/global.service';
import 'rxjs/add/operator/map';
import {GenericRestService} from '../../shared/services/generic-rest.service';
import { MzToastService } from 'ng2-materialize';

@Injectable()
export class SignalCodeService extends GenericRestService {

  public updateSubject = new Subject<any>();
  public headers = new Headers({ 'Content-type': 'application/json' });
  public options = new RequestOptions({ headers: this.headers });

  constructor(private globalService:GlobalService,protected http: Http, protected toastService: MzToastService) {
    super(http, toastService); 
   }

  getSignalCode() {
    return this.http.get('/SignalCodesWEB/api/rest/v1/signalcodes/getSignalCodes/'+this.globalService.getUsrConfig().user.selected_company).map(
      (res) =>
        res.json()
    )
  }

  getItems(customerPK){
    return this.http.get('/AspenConnectWEB/api/mdm/v1/headers/'+customerPK).map(
      (res) =>
        res.json()
    )

  }

  updateCode(data){
    return this.http.post('/SignalCodesWEB/api/rest/v1/signalcodes/updatesignalcodes', data, this.options).map(
      (res) => {
        this.updateSubject.next(data);
       this.handleAuth(res)
       this.updateSubject.next(data);
      }
   ).catch(err => this.handleError(err))
  }

}
