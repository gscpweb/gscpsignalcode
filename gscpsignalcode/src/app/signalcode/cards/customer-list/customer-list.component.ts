import { Component, OnInit, ViewChild } from '@angular/core';
import {SignalCodeService} from '../../shared/signal-code.service';
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { MzModalService } from 'ng2-materialize';
import {UpdateModalComponent} from '../../modals/update-modal/update-modal.component';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  public signalList:any;
  public signalListTemp:any;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  public selected_Signal:any;
  public itemList:any;
 
  constructor(private signal_Service:SignalCodeService,private modalService:MzModalService,public translate:TranslateService) { 
    translate.addLangs(["en"]);
    translate.setDefaultLang('en');
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en/) ? browserLang : 'en');
  }
  selectedCustomer(row){
    this.selected_Signal=row;
    this.modalService.open(UpdateModalComponent,{selected_Signal:this.selected_Signal});

  }
  FilterCustomersFrom(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.signalListTemp.filter(function (d) {
      return d.customerID.toLowerCase().indexOf(val) !== -1 || d.customerName.toLowerCase().indexOf(val) !== -1 || d.endMarketCountry.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.signalList = temp;
    this.table.offset = 0;
  }

  ngOnInit() {
    this.signal_Service.getSignalCode().subscribe(data=>{
      this.signalListTemp=data;
      this.signalList=data;
    })
    this.signal_Service.updateSubject.subscribe(data=>
      this.signal_Service.getSignalCode().subscribe(data=>this.signalList=data)
    );
  }

}
