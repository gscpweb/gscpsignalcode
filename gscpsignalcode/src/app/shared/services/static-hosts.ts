export class StaticHost {
    public ip: string = '';
    public devIP: string = '/GSCPBPMRSWeb/api/v1/bpm'
    public userURL: string = this.ip + '/GSCPUMWeb/api/um/v1/user/details';
    public menuURL: string = 'assets/data/getMenuStructure.json';
    public bpmTasksURL: string = this.devIP + '/getTasks';
    public bpmTaskById: string = this.devIP + '/getTasks';
    public completedTaskURL: string = this.devIP + '/getCompletedTasks';
    public taskDetailsURL: string = this.devIP + '/getTaskDetails';
    public actionURL: string = this.devIP + '/completeTask';
    

}