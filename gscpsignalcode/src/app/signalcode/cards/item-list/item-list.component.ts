import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MzModalService } from 'ng2-materialize';
import {UpdateModalComponent} from '../../modals/update-modal/update-modal.component';
import {SignalCodeService} from '../../shared/signal-code.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit,OnChanges {
  itemList:any;
  itemListTemp:any;
  @Input() selected_Customer:any;
  itemsIsShowed:boolean=true;
  selected_Item:any;

  constructor(private modalService:MzModalService,private signal_Service:SignalCodeService) { }
  selectedItem(row){
    this.selected_Item=row;
    console.log(this.selected_Item)
    this.modalService.open(UpdateModalComponent,{selected_Item:this.selected_Item});
  }
  showItems(){
    this.itemsIsShowed=false;
  }

  ngOnInit() {
   // console.log('hallaaaa Am gonna call the service')
    this.signal_Service.getItems(this.selected_Customer.customerPK).subscribe(data=>this.itemList=data);

    this.signal_Service.updateSubject.subscribe(data=>
      this.signal_Service.getItems(this.selected_Customer.customerPK).subscribe(data=>this.itemList=data)
    );
  }
  ngOnChanges(changes) {
 
  }

}
