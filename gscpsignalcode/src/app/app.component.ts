import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { GlobalService } from './shared/services/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  constructor(private gserv: GlobalService, private title: Title) { }

  ngOnInit() {
    console.log("AppComponent - OnInit");
    if (this.gserv.getAppConfig() == null) {
      this.gserv.setAppConfig();
    }
    const coreAppConfig = this.gserv.getAppConfig();
    this.title.setTitle(coreAppConfig.cfgAppTitle);
  }

}
