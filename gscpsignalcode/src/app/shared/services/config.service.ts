import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
 
@Injectable()
export class ConfigService {
 
  private _config: any;
 
  constructor(private http: Http) { }
 
  // Loads the config at start
  load(): Promise<any> {
    // console.log("ConfigService.load()");
    return this.http.get('./assets/data/app-config-base.json')
      .map((response: Response) => response.json())
      .toPromise()
      .then(data => {
        this._config = data;
        return data;
      }).catch(error => {
        console.log(error);
      });
  }
 
  // This will return the config that was loaded.
  get config(): any {
    return this._config;
  }
 
}
