import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalCodeContainerComponent } from './signal-code-container.component';

describe('SignalCodeContainerComponent', () => {
  let component: SignalCodeContainerComponent;
  let fixture: ComponentFixture<SignalCodeContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignalCodeContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignalCodeContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
