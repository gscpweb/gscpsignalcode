import { AppComponent } from './app.component';
import { BodyComponent } from './core/body/body.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ConfigService } from './shared/services/config.service';
import { CoreComponent } from './core/core.component';
import { DatePipe } from '@angular/common';
import { FooterComponent } from './core/footer/footer.component';
import { FormsModule } from '@angular/forms';
import { GlobalFunctionsService } from './shared/functions/global-functions.service';
import { GlobalService } from './shared/services/global.service';
import { HttpModule, Http } from '@angular/http';
import { MaterializeModule } from 'ng2-materialize';
import { ModalComponent } from './core/navbar/modal/modal.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { NavbarFunctionsService } from './shared/functions/navbar-functions.service';
import { NavbarService } from './shared/services/navbar/navbar.service';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule, Routes } from '@angular/router';
import { SharedComponent } from './shared/shared.component';
import { SidebarComponent } from './core/sidebar/sidebar.component';
import { UserConfigService } from './shared/services/user-config.service';
import { UserprefComponent } from './core/navbar/modal/userpref/userpref.component';
import { TranslateModule } from 'ng2-translate';
import { DisclaimerComponent } from './core/disclaimer/disclaimer.component';
import { SignalCodeContainerComponent } from './signalcode/cards/signal-code-container/signal-code-container.component';
import { CustomerListComponent } from './signalcode/cards/customer-list/customer-list.component';
import { ItemListComponent } from './signalcode/cards/item-list/item-list.component';
import {SignalCodeService} from './signalcode/shared/signal-code.service';
import { UpdateModalComponent } from './signalcode/modals/update-modal/update-modal.component';
//import { CrfConfigService } from './shared/services/orderbook/crf-config.service';
export const appRoutes: Routes = [
  { path: '', component: SignalCodeContainerComponent},
  { path: '#/signalcode', component: SignalCodeContainerComponent }
];

// Do initing of services that is required before app loads configService.load()
// NOTE: this factory needs to return a function (that then returns a promise)
export function init_app_cfg(configService: ConfigService) {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      return configService.load()
        .then((data: any) => {
          console.log('[object Promise] init_app_cfg :: resolved');
          resolve(data);
        })
        .catch((err: any) => {
          console.error(err);
          Promise.reject(err);
        });
    });
  };
};

// export function init_app_crf(crf_config_serice: CrfConfigService) {
//   return (): Promise<any> => {
//     return new Promise((resolve, reject) => {
//       return crf_config_serice.load()
//         .then((data: any) => {
//           console.log('[object Promise] init_app_crf :: resolved');
//           resolve(data);
//         })
//         .catch((err: any) => {
//           console.error(err);
//           Promise.reject(err);
//         });
//     });
//   };
// };

export function init_app_usr(userv: UserConfigService) {
  // Do initing of services that is required before app loads
  // NOTE: this factory needs to return a function (that then returns a promise)
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      return userv.load()
        .then((data: any) => {
          console.log('[object Promise] init_app_usr :: resolved');
          resolve(data);
        })
        .catch((err: any) => {
          console.error(err);
          Promise.reject(err);
        });
    });
  };
}

@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    CoreComponent,
    FooterComponent,
    NavbarComponent,
    SharedComponent,
    SidebarComponent,
    ModalComponent,
    UserprefComponent,
    DisclaimerComponent,
    SignalCodeContainerComponent,
    CustomerListComponent,
    ItemListComponent,
    UpdateModalComponent

  ],
  entryComponents: [UserprefComponent,UpdateModalComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    TranslateModule.forRoot(),
    MaterializeModule.forRoot(),
    NgbModule.forRoot(),
    NgxDatatableModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot()
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: init_app_cfg,
      deps: [ConfigService],
      multi: true
    },
    DatePipe,
    GlobalFunctionsService,
    GlobalService,
    NavbarFunctionsService,
    NavbarService,
    UserConfigService,
    SignalCodeService,
    {
      provide: APP_INITIALIZER,
      useFactory: init_app_usr,
      deps: [UserConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
