import { Component, OnInit, ViewChild, Pipe, PipeTransform, AfterViewInit, QueryList, ElementRef, forwardRef, ViewChildren, ContentChild } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Router, ActivatedRoute } from "@angular/router";

import { stringToArrayBuffer } from "@angular/http/src/http_utils";
import { DatePipe } from '@angular/common';
import { GlobalService } from '../../shared/services/global.service';
import { DatatableComponent } from '@swimlane/ngx-datatable'
@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  public landingPage: boolean = true;
  public detailsPage: boolean = false;
  private endPoint: String = "";
  constructor(
    public datepipe: DatePipe,
    private globalService: GlobalService,
    private router: Router,
    private actroute: ActivatedRoute
  ) { }
  //switch pages
  togglePages() {
    this.detailsPage = this.globalService.tglDetailsPage();
    this.landingPage = !this.detailsPage;
  }
  ngOnInit(): void {
    const bodyAppConfig = this.globalService.getAppConfig();
    this.endPoint = window.location.href.replace("%23", "#");
    if (this.endPoint.indexOf('#') > 0) {
       if (this.endPoint.indexOf('bpm') > 0) {
        this.router.navigate(['/#/bpm']);
      } else if (this.endPoint.indexOf('bpm') > 0) {
        var query: any;
        if (this.endPoint.indexOf(";") > 0) {
          query = { odbNumber: 0 };
          this.router.navigate(['/#/bpm', query]);
        } else
          this.router.navigate(['/#/bpm']);
      } else if (this.endPoint.indexOf('crfmatrix') > 0) {

        this.router.navigate(['/#/crfmatrix']);
      } else {
        window.location.href = "" + this.endPoint.replace("/GSCPBPMWeb", "");
      }
    }
   else{
       window.location.href = "" + this.endPoint.replace("/GSCPBPMWeb", "");

    }
  }



}
