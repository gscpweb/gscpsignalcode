import { OrderbookPage } from './app.po';

describe('orderbook App', () => {
  let page: OrderbookPage;

  beforeEach(() => {
    page = new OrderbookPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
