import { Component, OnInit, Input } from '@angular/core';
import { MzBaseModal, MzModalComponent, MzToastService } from 'ng2-materialize';
import {SignalCodeService} from '../../shared/signal-code.service';
import { GlobalService } from '../../../shared/services/global.service';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-update-modal',
  templateUrl: './update-modal.component.html',
  styleUrls: ['./update-modal.component.css']
})
export class UpdateModalComponent extends MzBaseModal implements OnInit  {

  @Input() selected_Signal:any;
  public signalAction:any=[{value:"2",action:true},{value:"1",action:false}]

  public Visible:any;
  public sendFcErp:any;
  public existingOrders:any;
  public newOrders:any;
  public salesUpload:any;
  public inventoryUpload:any;

  constructor(private signal_Service:SignalCodeService,private toastService:MzToastService,private globalService : GlobalService, private translate:TranslateService) { 
    super();
    translate.addLangs(["en"]);
    translate.setDefaultLang('en');
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en/) ? browserLang : 'en');
  }
  updateCodes(){
    var signalObj={
      "visible":this.Visible,
      "sendfcerp":this.sendFcErp,
      "existingorders":this.existingOrders,
      "createnewordersallowed":this.newOrders,
      "salesupload":this.salesUpload,
      "inventoryupload":this.inventoryUpload,
      "businessentityPk":this.globalService.getUsrConfig().user.selected_company,
      "signalcodeid":this.selected_Signal.signalcodeid,
      "signalcode":this.selected_Signal.signalcode,
      "signalcodePk":this.selected_Signal.signalcodePk
    }
    this.signal_Service.updateCode(signalObj).subscribe(data=>{
      this.toastService.show('Signal Code Updated!', 5000, 'green');
    })
   
  }

  ngOnInit() {
    this.Visible=this.selected_Signal.visible;
    this.sendFcErp=this.selected_Signal.sendfcerp;
    this.existingOrders=this.selected_Signal.existingorders;
    this.newOrders=this.selected_Signal.createnewordersallowed;
    this.salesUpload=this.selected_Signal.salesupload;
    this.inventoryUpload=this.selected_Signal.inventoryupload;
  
  }

}
