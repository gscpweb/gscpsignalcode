import { TestBed, inject } from '@angular/core/testing';

import { SignalCodeService } from './signal-code.service';

describe('SignalCodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SignalCodeService]
    });
  });

  it('should be created', inject([SignalCodeService], (service: SignalCodeService) => {
    expect(service).toBeTruthy();
  }));
});
