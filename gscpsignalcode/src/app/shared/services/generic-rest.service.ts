import { Injectable } from '@angular/core';
import { Headers, Http, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { MzToastService} from 'ng2-materialize';

@Injectable()
export class GenericRestService {
    constructor(protected http: Http, protected toastService: MzToastService) { }

    handleAuth(resp) {
        //console.warn(resp);
        if (resp == null) {
            return;
        }
        if (resp.status === 200) {
            const obj = resp.json();
            if (obj != null && obj.code && obj.code === 401) {
                // If we are not logged in abandon everything and login again
                console.error('Not authorized');
                window.location.href = '/../i1Login/loggedin.jsp?url=/GSCPWeb/';
            }
            return obj;
        } else if (resp.code === 401) {
            console.error('Not authorized');
            window.location.href = '/../i1Login/loggedin.jsp?url=/GSCPWeb/login';
        } else {
            console.log('Something else is going on');
        }
    }

      handleError(error) {
        console.error('Rest error: ', error);
        if (error.status === 401) {
            return Observable.throw('Not Authorized');
        }else if (error.status === 500) {
            this.toastService.show('Something went wrong!!!', 5000, 'red');
            return Observable.throw('Please handle this error! Server is unhappy');
        }else if  (error.status === 303) {
            const obj = error.json();
            this.toastService.show(obj.businessError, 5000, 'red');
            return Observable.throw('Please handle this error! Server is unhappy');
        }
        const obj = error.json();
        this.toastService.show(obj.businessError, 5000, 'red');
        return Observable.throw('Please handle this error');
    }
}