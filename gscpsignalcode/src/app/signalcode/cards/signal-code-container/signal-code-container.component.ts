import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../../../shared/services/global.service';

@Component({
  selector: 'app-signal-code-container',
  templateUrl: './signal-code-container.component.html',
  styleUrls: ['./signal-code-container.component.css']
})
export class SignalCodeContainerComponent implements OnInit {

  constructor(private globalService:GlobalService) { }

  userManagement(){
    window.location.href = '/GSCPUMUIWeb/#/um?company='+this.globalService.getUsrConfig().user.selected_company;
  }
  siteLog() {
    window.location.href = '/GSCPLOCKWeb/#/administration?company='+this.globalService.getUsrConfig().user.selected_company;
  }
  forecastLock() {
    window.location.href = '/GSCPLOCKWeb/#/forecast?company='+this.globalService.getUsrConfig().user.selected_company;
  }
  itemTranfer() {  
    window.location.href = '/GSCPTRANSFERWeb/#/itemtransfer?company='+this.globalService.getUsrConfig().user.selected_company;
  }
  signalCode() {  
    window.location.href = '/GSCPSIGNALCODEWeb/#/signalcode?company='+this.globalService.getUsrConfig().user.selected_company;
  }


  ngOnInit() {
    window.history.pushState("object or string", "Title", "/GSCPSIGNALCODEWeb/#/signalcode");
  }

}
