import { Injectable } from '@angular/core';
import { Http, RequestOptions,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {GenericRestService} from '../generic-rest.service';
import { MzToastService } from 'ng2-materialize';
import { StaticHost } from '../static-hosts';

@Injectable()
export class NavbarService extends GenericRestService {
  public staticHost: StaticHost = new StaticHost();
  private headers = new Headers({ 'Content-type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });
  constructor(protected http: Http, protected toastService: MzToastService) { 
    super(http, toastService);
  }
 
  getBusinessEntities() {
     return this.http.get(this.staticHost.userURL).map(
      (res) => {
         this.handleAuth(res);
       }
     ).catch(err => this.handleError(err))
  }

  getUserPreference(userPK){
    return this.http.get('/AspenConnectWEB/api/um/v1/'+userPK+'/details').map(
      (res) => res.json()
    )

  }

  //get the menu bar
  getMenuItems() {
     return this.http.get(this.staticHost.menuURL).map(
      (res) => {
         this.handleAuth(res);
       }
     ).catch(err => this.handleError(err))
  }

  updateUserPref(data){
  //  /GSCPUMWeb/api/um/v1/user/updateUserPreferences
  return this.http.post('/GSCPUMWeb/api/um/v1/user/updateUserPreferences',data, this.options).map(
      (res) => {
         this.handleAuth(res);
       }
     ).catch(err => this.handleError(err))
  }

  //get the menu bar
  getCompanies() {
    return this.http.get('assets/data/getUserBusinessDetails.json').map(
      (res) => {
         this.handleAuth(res);
       }
     ).catch(err => this.handleError(err))
  }
}
